#include <stdio.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <errno.h>

#define PORT 9999
#define BUFFER_SIZE 128
#define MAX_SEQUENCE 10 //so eventually it stops

typedef struct message_format{//packet format
  int id;
  int sequence;
}message_format;


void input_function(char* buf, int msg_len, int socket);
void output_function(const char* buf, int msg_len, int socket, const struct sockaddr_in* address);
int is_close(int id, const char* close_list[], int length); //returns true if the id belongs to a "close" node

int main(int argc, char* argv[]){
  setvbuf(stdout, NULL, _IONBF, 0);

  if(argc < 3){
    printf("too few arguments for program\n");
    return -1;
  }  
  
  struct sockaddr_in server_address = {0};//address to send/receive
  struct sockaddr_in broadcast = {0}; //address to send to 
  int sock;
  socklen_t address_len = sizeof(struct sockaddr_in);

  int ret;
  int yes = 1;

  int sequence = 0;
  char buf[BUFFER_SIZE];
  int msg_len;
  
  int sender_id;
  int sender_sequence;
  int ignore_message = 0;

  int id = atoi(argv[1]);
   
  printf("node: %d started\n", id);

  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = htons(INADDR_ANY);
  server_address.sin_port = htons(PORT);
  
  broadcast.sin_family = AF_INET;
  broadcast.sin_addr.s_addr = htonl(INADDR_BROADCAST);
  broadcast.sin_port = htons(PORT);
  
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if(0 > socket){
    printf("error: socket()\n");
    return -1;
  }

  ret = setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (char*) &yes, sizeof(yes));
  
  ret = bind(sock, (struct sockaddr*) &server_address, address_len);
  if(0 > ret){
    printf("error: bind()\n");
    return -1;
  }
  
  //snprintf(buf, BUFFER_SIZE, MESSAGE_FORMAT, id, sequence); //message format is
  message_format message = {.id = id, .sequence = sequence};  
  msg_len = sizeof(message_format);
  memcpy((void*)buf, (void*)&message, msg_len);
  
  if( id == 0){
    //we are the leader, send first message
    sleep(2); //make sure the others have started
    output_function(buf, msg_len, sock, &broadcast);
    sequence++;
  }
  
  int over = 0;
  while(!over){
    //receive/send cicle 
    memset(buf, 0, BUFFER_SIZE);//clean the buffer
    input_function(buf, msg_len, sock);
     
    ignore_message = 0;
    message = *((message_format*)buf);
    sender_id = message.id;
    sender_sequence = message.sequence;
    printf("received message from node: %d, seq: %d\n",sender_id, sender_sequence);
   
    if(sender_id == id){
      ignore_message = 1;
      printf("own message, ignored\n");      
    }
    
    else if(sender_sequence < sequence){
      ignore_message = 1;
      printf("message not in sequence, ignored\n");    
    }
    
    else if(!is_close(sender_id, (const char**)argv + 2, argc - 2)){
      ignore_message = 1;
      printf("message from distant node, ignored\n");
    }
    
    if(!ignore_message){
      if(sequence == sender_sequence){
        sequence++;
      }
      if(sender_sequence >= MAX_SEQUENCE){
        over = 1;
      }

      memset(buf, 0, BUFFER_SIZE);//clean the buffer
      message.id = id;
      message.sequence = sender_sequence + 1;
      memcpy((void*)buf, (void*)&message, msg_len);

      output_function(buf, msg_len, sock, &broadcast);
    }
    if(sequence >= MAX_SEQUENCE){//just to stop eventually
      over = 1;
    }
  }
  printf("shutting down\n");
  close(sock);
  return 0;
}

void input_function(char* buf, int msg_len, int socket){
    int bytes_received = 0;
    socklen_t address_len = sizeof(struct sockaddr_in);
    int ret = 0;
    struct sockaddr_in fool;
    
    while(bytes_received < msg_len){
      
      ret = recvfrom(socket, (void*) buf + bytes_received, msg_len - bytes_received, 0, (struct sockaddr*)&fool, &address_len);//expect a message in the same format of the one we sent, sender address is not needed, messages are broadcast
      
      if(ret == -1){
        if(errno == EINTR){
          continue;
        }
        else{
          printf("error: recvfrom\n");
          exit(-1);
        }
      }
      if(ret == 0){
        printf("error: 0 bytes returned\n");//not sure how to manage this case, shut down seems too drastic
        break;
      }
      bytes_received += ret;
    
    }
}

void output_function(const char* buf, int msg_len, int socket, const struct sockaddr_in* address){
  int bytes_sent = 0;
  socklen_t address_len = sizeof(struct sockaddr_in);
  int ret = 0;
  
  while(bytes_sent < msg_len){
    ret = sendto(socket, (void*)buf + bytes_sent, msg_len - bytes_sent, 0, (struct sockaddr*) address, address_len);//broadcast
    
    if(ret == -1){
      if(errno == EINTR){
        continue;
      }
      else{
        printf("error: sendto()\n");
        exit(-1);
      }
    }
    bytes_sent += ret;
    
  }
}

int is_close(int id, const char* close_list[], int lenght){
  const char** aux = close_list; 
  int L = lenght;
  int i;
  for(i = 0; i < L; i++){
    if(id == atoi(*aux)){
      return 1;
    }
    aux++; 
  }
  return 0;  
}